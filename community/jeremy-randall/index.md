---
path: 'jeremy-randall'
name: 'Jeremy Randall'
tagline: '#Software developer'
avatar: 'deremije.jpg'
role: 'developer'
---

#Hi

I'm a self-taught developer looking for interesting projects to work on. I work in HTML, CSS, JavaScript, Node, React, Python, and whatever you need me to learn in order to code.

### My links

**Website:** <a href="http://www.deremije.com" target="_blank">Đeremije.com</a>, my portfolio site which I neglect in order to work on other stuff.
