---
path: 'asif-kabani'
name: 'Asif Kabani'
tagline: 'Front End Developer & UI Designer'
avatar: './asif.jpg'
role: 'developer'
---

Hi I am Asif Kabani, a Front End Developer with expertise in HTML/CSS & JavaScript. Also I am aspiring to be a UI Designer.

### Technologies

- HTML/CSS
- JavasSript
- React
- Bash
- Adobe XD, Sketch, Invision

### Profiles and sites

- **Website:** <a href="https://asifkabani.com/" target="_blank">Asif Kabani</a>
- **Github:** <a href="https://github.com/asifkabani" target="_blank">asifkabani</a>
- **Gitlab:** <a href="https://gitlab.com/asifkabani" target="_blank">asifkabani</a>
