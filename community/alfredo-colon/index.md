---
path: 'alfredo-colon'
name: 'Alfredo Colón'
tagline: 'Cloud Software Engineer'
avatar: './avatar.jpg'
role: 'mentor'
gitlab: 'MarketingLinkCTO'
---
Passionate software engineer that loves to teach and learn.
