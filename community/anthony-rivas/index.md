---
path:    "anthony-rivas"
name:    "Anthony Rivas"
tagline: "Full-Stack Developer and Teacher"
avatar:  "./profile-image.jpg"
role:    "mentor"
---

Hey everyone. I am a full-stack developer and also an instructor for the University of Arizona Coding Bootcamp. I have a passion for well written, maintainable, and beautiful JavaScript (Just don't verify this with my hacking around on Github 😂).

When I'm not coding, I can be found with my family, listening to music, and playing videogames.

I am also the co-host of a realtively new , and very inconsistently recorded podcast called K&&R: BinaryView

### Top Technologies

- JavaScript / Node
- HTML / CSS
- React
- PHP
- Azure SQL
- MongoDB

### Profiles and sites

- **Website:** <a href="http://aerivas.com/" target="_blank">Anthony Rivas</a>
- **Soundcloud:** <a href="https://soundcloud.com/binary-view" target="_blank">K&&R: BinaryView Podcast</a>
- **Github:** <a href="https://github.com/anthonyrivas" target="_blank">anthonyrivas</a>
- **Gitlab:** <a href="https://gitlab.com/anthonyrivas" target="_blank">anthonyrivas</a>
