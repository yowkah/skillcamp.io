---
path:    "michael-daffin"
name:    "Michael Daffin"
tagline: "Backend developer and DevOps engineer"
avatar:  "./mdaffin.jpg"
role:    "mentor"
---

Hey! I am a backend focused developer with a prefence towards automation tooling and general DevOps practices. Most of my work is in Rust and Go but I make use of javascript when needed and I know far too much about bash scripting.

If you have any questions feel free to ask, @Michael on the SkillCamp slack channel or @mdaffin on gitlab and github.

### Top Languages

- Rust
- Go
- Javascript
- Python
- Bash

### Profiles and sites

* **Website:** <a href="https://disconnected.systems/" target="_blank">Disconnected Systems</a>
* **Github:** <a href="https://github.com/mdaffin" target="_blank">mdaffin</a>
* **Gitlab:** <a href="https://gitlab.com/mdaffin" target="_blank">mdaffin</a>