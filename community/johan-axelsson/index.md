---
path: 'johan-axelsson'
name: 'Johan Axelsson'
tagline: 'Frontend Developer'
avatar: './johan-axelsson.jpg'
role: 'mentor'
gitlab: 'nerdic-coder'
---

Y'ello!

I'm a Frontend Application Developer on Åland Island, Finland.
I work at a company called Crosskey and I'm in the team that develop mobile banking applications.
Our tech stack includes Angular, Ionic, Cordova, NodeJS, Git, Jenkins and Java.

Just hit me up on any social channel that I'm available at if you need any help with frontend development issues or questions.

### Top Languages

- Javascript
- Typescript
- Java
- PHP

### Profiles and sites

- **Website:** <a href="https://nerdic-coder.com/" target="_blank">Nerdic Coder Blog</a>
- **Github:** <a href="https://github.com/nerdic-coder" target="_blank">nerdic-coder</a>
- **GitLab:** <a href="https://gitlab.com/nerdic-coder" target="_blank">nerdic-coder</a>
- **Linkedin:** <a href="https://www.linkedin.com/in/johax/" target="_blank">johax</a>
- **Twitter:** <a href="https://twitter.com/nerdic_coder" target="_blank">nerdic_coder</a>
- **Instagram:** <a href="https://www.instagram.com/nerdic.coder/" target="_blank">nerdic.coder</a>
- **Facebook:** <a href="https://www.facebook.com/nerdic.coder" target="_blank">nerdic.coder</a>
