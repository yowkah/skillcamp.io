import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import Header from '../components/Header'
import Footer from '../components/Footer'
import './index.css'
import favicon_32 from '../favicon/favicon-32x32.png'
import favicon_16 from '../favicon/favicon-16x16.png'
import apple_touch from '../favicon/apple-touch-icon.png'
import safari_pinned from '../favicon/safari-pinned-tab.svg'

const Wrapper = styled.div`
  min-height: 100vh;
  margin-top: 60px;

  @media screen and (max-width: 600px) {
    margin-top: 0;
  }
`

const Layout = ({ children, data, location }) => (
  <div>
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    >
      {/*Favicon Stuff*/}
      <link rel="apple-touch-icon" sizes="180x180" href={apple_touch} />
      <link rel="icon" type="image/png" sizes="32x32" href={favicon_32} />
      <link rel="icon" type="image/png" sizes="16x16" href={favicon_16} />
      <link rel="mask-icon" href={safari_pinned} color="#fbc15a" />
      <meta name="msapplication-TileColor" content="#fbc15a" />
      <meta name="theme-color" content="#fbc15a" />
    </Helmet>
    <Header siteTitle={data.site.siteMetadata.title} location={location} />
    <Wrapper>{children()}</Wrapper>
    <Footer />
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
