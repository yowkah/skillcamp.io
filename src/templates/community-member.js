import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import Img from 'gatsby-image'

const Div = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  max-width: 1440px;
  margin: 120px auto 60px auto;

  @media screen and (min-width: 1440px) {
    margin: 100px auto;
  }

  @media screen and (max-width: 601px) {
    margin: 75px auto;
  }
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  margin: 20px;
  max-width: 1440px;

  @media screen and (min-width: 1440px) {
    margin: 20px auto;
  }
`

const Heading = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  margin: 0 auto 50px;
`

const Avatar = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 26.1px;

  div {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  p {
    margin: 0;
    font-size: 17px;
    font-weight: 600;
  }

  span {
    color: #888;
    font-size: 14px;
    font-weight: 400;
  }
`

const AvatarImage = styled(Img)`
  margin: 0 10px 0 0;
  height: 60px;
  width: 60px;
  border-radius: 50%;
`

const Name = styled.div`
  font-weight: 600;
  font-size: 40px;
  margin-bottom: 10px;
`

const Tagline = styled.div`
  font-size: 15px;
  margin-bottom: 2px;
  color: #777;
`

const Role = styled.div`
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 20px;
  color: #777;
`
const About = styled.div`
  width: 700px;

  @media (max-width: 850px) {
    width: 100%;
  }
`

const CommunityMemberLayout = ({ data, location }) => {
  const { markdownRemark: member } = data
  const { frontmatter, html } = member
  const { name, role, avatar, tagline } = frontmatter

  return (
    <Div>
      <Wrapper>
        <Heading>
          <Avatar>
            <AvatarImage resolutions={avatar.childImageSharp.resolutions} />
          </Avatar>
          <Name>{name}</Name>
          <Tagline>{tagline}</Tagline>
          <Role>{role}</Role>
        </Heading>
        <About>
          <div dangerouslySetInnerHTML={{ __html: html }} />
        </About>
      </Wrapper>
    </Div>
  )
}

export const memberQuery = graphql`
  query CommunityMemberByPath($_path: String!) {
    markdownRemark(frontmatter: { path: { eq: $_path } }) {
      html
      frontmatter {
        name
        tagline
        role
        path
        avatar {
          childImageSharp {
            resolutions(width: 200, height: 200, quality: 90) {
              ...GatsbyImageSharpResolutions
            }
          }
        }
      }
    }
  }
`

export default CommunityMemberLayout
